from flask import Flask, jsonify, request
from typing import List, Dict
import mysql.connector
import json

app = Flask(__name__)

restaurantes=[
	{"idRestaurante":1,"nombre":"Restaurante GT"},
	{"idRestaurante":2,"nombre":"Restaurante Flores"}
]
pedidos={
	"1":{"idUsuario":100,"idRestaurante":1,"estadoPedido":"Preparando","producto":"Comida A","cantidad":3},
	"2":{"idUsuario":200,"idRestaurante":2,"estadoPedido":"Listo","producto":"Comida B","cantidad":1},
	"3":{"idUsuario":300,"idRestaurante":1,"estadoPedido":"En Camino","producto":"Comida B","cantidad":5}
}

@app.route('/recibirPedido',methods=['POST'])
def recibirPedido():
	return recibirPedidoYGuardar(request.json['idUsuario'],request.json['producto'],request.json['cantidad'],request.json['idRestaurante'])

def recibirPedidoYGuardar(idUsuario,producto,cantidad,idRestaurante):
	#Generar un nuevo registro de un Pedido
	nuevoPedido={
		"idUsuario":idUsuario,
		"estadoPedido":"En Cola",
		"producto":producto,
		"cantidad":cantidad,
		"idRestaurante":idRestaurante
	}
	
	#Generar el ID del Pedido
	idPedido=len(pedidos)+1

	#Guardar el pedidod en el diccionario de pedidos
	pedidos[str(idPedido)]=nuevoPedido

	#Retornar la informacion del pedido
	return {"mensaje":'El pedido fue recibido',"idPedido":idPedido,}

@app.route('/estadoPedido/<int:idPedido>',methods=['GET'])
def estadoPedido(idPedido):
	return {"estadoPedido":estadoPedidoValue(idPedido)}

def estadoPedidoValue(idPedido):
	if str(idPedido) in pedidos:
		respuesta=pedidos[str(idPedido)]
		return respuesta['estadoPedido']
	return 'No existe el pedido'

@app.route('/repartidorPedidoListo/<int:idPedido>',methods=['GET'])
def repartidorPedidoListo(idPedido):	
	return isPedidoListo(idPedido)


def restaurantes() -> List[Dict]:
    config = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'bdsa'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM restaurantes')
    html='<table> <tbody>'
    for (nombres, telefono) in cursor:
        html+='<tr> <td>'+nombre+'</td> <td>'+ telefono +'</td> </tr>'
    html+='</tbody> </table>'
    #results = [{'Nombres':nombres,'Correo':correo,'carnet': carnet} for (nombres, correo,carnet) in cursor]
    cursor.close()
    connection.close()

    return html

@app.route('/restaurantes')
def index() -> str:
    return restaurantes()
	
def isPedidoListo(idPedido):
	if str(idPedido) in pedidos:
		respuesta=pedidos[str(idPedido)]
		if respuesta['estadoPedido']=='Listo':
			return {"Listo":"SI"}
		else:
			return {"Listo":"NO"}
	return {"Listo":'No existe el pedido'}


if __name__ == '__main__': 
	app.run(debug="True",host='0.0.0.0', port=4002)