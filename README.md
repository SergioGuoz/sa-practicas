# SoftwareAvanzado
### Practica 10

# Docker y DockerCompose

Cada uno de los microservicios corre en docker.

## Microservicios:
- ESB
- Repartidor
- Cliente
- Restaurante 
    
# Gitlab CI CD

Gitlab da la facilidad de integrar integracion continua y despliegue continuo.

Corre las pruebas que tenemos segun le indiquemos en la carpeta donde se encuentren.

```yml
cache:
    key: "$CI_COMMIT_REF_NAME ubuntu:16.04"
    paths:
    - imagenes/

stages:
  - Build
  - Test
  - Deploy  #CD - Continuos Deploy

install:
    stage: Build
    image: python:3.8-slim-buster
    script:
    - echo "Fase de install"
    artifacts:
        paths:
          - imagenes/
    cache:
        paths:
          - imagenes/

test:
    stage: Test
    image: python:3.8-slim-buster
    before_script:
      - ls
      - cd ./restaurante
      - pip install -r requirements.txt 
    script:
        - echo "Test"
        - python test.py 


desplegarRestaurante:
    stage: Deploy
    image: ubuntu:16.04
    environment:
      name: production
      url: 'http://35.224.10.195'
    before_script:
      - ls
      - cd ./restaurante
      - apt update && apt-get install -y gnupg2
      - apt install curl -y
      - apt-get install apt-transport-https ca-certificates software-properties-common -y
      - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
      - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
      - apt-get update
      - apt-get install docker-ce docker-ce-cli containerd.io -y
      - apt-get install python3-pip python-pip -y
      - pip install -r requirements.txt # instalando dependencias
      - curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
      - chmod +x /usr/local/bin/docker-compose
      - ls /usr/local/bin/docker-compose
      - service docker start
      - docker-compose -version

    script:
        - echo "Release"
        - docker-compose up -d --build # docker-compose build
```

## Base de Datos
Se usa una base de datos MySQL y se une por medio de un docker-compose con la aplicación de restaurante.

## Pruebas Unitarias

Importamos librerías, unittest para las pruebas y restaurante es la clase a la que realizaremos las pruebas.
```python
import unittest
import restaurante 
```

### Pruebas
Le asignamos un nombre que especifique lo que la prueba esta haciendo y luego llamamos al metodo assert para comprobar los resultados 
y compararlo con los valores que deberían retornar para pasar la prueba.

```python

class TestRestaurante(unittest.TestCase):


    def test_estado_pedido_invalido(self):
        self.assertEqual(restaurante.estadoPedidoValue(-1),'No existe el pedido')

```



## PRUEBAS DE FUNCIONALIDAD 🛠️
[YouTube] https://youtu.be/0mzqM2GkmX4
 
## Autor ✒️

* **Sergio Geovany Guoz Tubac** 🤓
